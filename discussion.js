// MongoDB Aggregation

db.course_bookings.insertMany([
    {
        "courseId" : "C001", 
        "studentId": "S004", 
        "isCompleted": true
    },
    {
        "courseId" : "C002", 
        "studentId": "S001", 
        "isCompleted": false
    },
    {
        "courseId" : "C001", 
        "studentId": "S003", 
        "isCompleted": true
    },
    {
        "courseId" : "C003", 
        "studentId": "S002", 
        "isCompleted": false
    },
    {
        "courseId" : "C001", 
        "studentId": "S003", 
        "isCompleted": true
    },
    {
        "courseId" : "C004", 
        "studentId": "S004", 
        "isCompleted": false
    },
    {
        "courseId" : "C002", 
        "studentId": "S007", 
        "isCompleted": true
    },
    {
        "courseId" : "C003", 
        "studentId": "S005", 
        "isCompleted": false
    },
    {
        "courseId" : "C001", 
        "studentId": "S008", 
        "isCompleted": true
    },
    {
        "courseId" : "C004", 
        "studentId": "S0013", 
        "isCompleted": false
    }
]);

/* db.collectionName.aggregate([ 
        {
            $group: {
                _id: <expression>, <"field">: {<accumulator>: <expression1>}
            }
        }
]) 
 aggregation Allows us to retrieve a group of data based on specific conditions. In this case, we are retrieving or grouping the data inside our course_booking table
*/

db.course_bookings.aggregate([
    {
        $group: {_id: null, count: {$sum: 1}}
    }
])

/* $match
    - is a condition that has to be met in order for MongoDB to return the data. In this case, we are trying to get all the fields where "isCompleted" is equal to "true".*/
db.course_bookings.aggregate([
    {
        $match: {"isCompleted": true}
    },
    {
        $group: {_id: "$courseId", total:{$sum: 1}}
    }
])

/* $project
    - basically either shows or hide a field (to include, use 1; exclude, use 0)
*/
db.course_bookings.aggregate([
    {
        $match: {"isCompleted": true}
    },
    {
        $project: {"studentId": 1}
    }
])

/* $sort 
    - sort organizes the returned data/ documents in ascending(1) or descending order (-1)*/
db.course_bookings.aggregate([
    {
        $match: {"isCompleted": true}
    },
    {
        $sort:{"courseId": -1}
    }
])

/*Mini Activity:
        Aggregate the documents in course_booking collections using 2 pipeline stages:
            Item 1: 
                a. In stage 1, use the $match operator to get all documents with true value in isCompleted field.
                b. in stage 2, use the $group operator, assign an _id to the new set of documents and count the total of those who completed the course.
            
            Item 2:
                a. In stage 1, use the $match operator to get all documents with true value in isCompleted field.
                b. in stage 2, use the $sort operation to arrange the courseId in descending order while the studentId in ascending order.*/

db.course_bookings.aggregate([
    {
        $match: {"isCompleted": true}
    },
    {
        $group: {
            _id: '$studentId',total:{$sum: 1}}      
    }
])

db.course_bookings.aggregate([
    {
        $match: {"isCompleted": true}
    },
    {
        $sort:{"courseId": -1, "studentId":1}
    }
])

db.orders.insertMany([
    {
        "customer_Id": "A123",
        "amount": 500,
        "status": "A"
    },
    {
        "customer_Id": "A123",
        "amount": 250,
        "status": "A"
    },
    {
        "customer_Id": "B212",
        "amount": 200,
        "status": "A"
    },
    {
        "customer_Id": "B212",
        "amount": 200,
        "status": "D"
    },
]);

// operators are also way to do automatic calculation within our query
/*  $ sum 
    - returns a sum or numerical values and ignores non-numeric values*/
/*  $max
    - returns the highest expression/ value for each group of documents*/
db.orders.aggregate([
    {
        $match: {"status": "A"}
    },
    {
        $group: {_id: '$customer_Id', maxAmount: {$max: "$amount"}}
    }
])

/* $min
    - returns the lowest expression / values for each group of documents*/
    db.orders.aggregate([
        {
            $match: {"status": "A"}
        },
        {
            $group: {_id: '$customer_Id', minAmount: {$min: "$amount"}}
        }
])

/* $avg
    -Returns an average of the numerical values. also, ignores non-numeric values*/
    db.orders.aggregate([
        {
            $match: {"status": "A"}
        },
        {
            $group: {_id: '$customer_Id', avgAmount: {$avg: "$amount"}}
        }
    ])

/* Mini Activity:
    Add these entries in your orders collection:
*/
    db.orders.insertMany([
    {
        "customer_Id": "A123",
        "amount": 550,
        "status": "D"
    },
    {
        "customer_Id": "B212",
        "amount": 300,
        "status": "A"
    },
    {
        "customer_Id": "B212",
        "amount": 400,
        "status": "D"
    },
    {
        "customer_Id": "A123",
        "amount": 350,
        "status": "A"
    }
]);
/* Solve:

    1.) Use the max operator to get the highest amount in orders with status D.

    2.) Use the average operator to get the average amount of orders in status A.*/
db.orders.aggregate([
    {
        $match: {"status": "D"}
    },
    {
        $group: {_id: '$status', maxAmount: {$max: "$amount"}}
    }
])

db.orders.aggregate([
    {
        $match: {"status": "A"}
    },
    {
        $group: {_id: '$status', avgAmount: {$avg: "$amount"}}
    }
])

